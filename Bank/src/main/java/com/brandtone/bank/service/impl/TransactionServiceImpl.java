/**
 * 
 */
package com.brandtone.bank.service.impl;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.brandtone.bank.constant.TransactionType;
import com.brandtone.bank.dao.GenericDAO;
import com.brandtone.bank.dao.TransactionDAO;
import com.brandtone.bank.entity.Account;
import com.brandtone.bank.entity.Transaction;
import com.brandtone.bank.exception.InsufficientFundsException;
import com.brandtone.bank.exception.UnrecognizedEntityException;
import com.brandtone.bank.service.AccountService;
import com.brandtone.bank.service.TransactionService;

/**
 * @author Ricardo Siqueira
 *
 */
@Service
@Qualifier("transactionServiceImpl")
public class TransactionServiceImpl extends GenericServiceImpl<Transaction> implements TransactionService {

	TransactionDAO dao;

	@Autowired
	public TransactionServiceImpl(@Qualifier("transactionDAO") GenericDAO<Transaction> superDao) {
		super(superDao);
		dao = (TransactionDAO)superDao;
	}

	public TransactionServiceImpl(){

	}

	@Autowired
	AccountService accountService;

	@Override
	// @Transactional //Make along with a future persistence layer
	public Transaction create(Transaction transaction) throws InsufficientFundsException {
		if(transaction==null)
			return null;
		try {
			if(transaction.getType() == TransactionType.TRANSFER)
				transfer(transaction);
			else
				deposit(transaction);
		} catch (UnrecognizedEntityException e) {
			e.printStackTrace();
		}

		return create(transaction);
	}

	/**
	 * actual business rule for a transfer
	 * @param transaction
	 * @throws InsufficientFundsException 
	 * @throws UnrecognizedEntityException 
	 */
	private void transfer(Transaction transaction) throws InsufficientFundsException, UnrecognizedEntityException{
		Account from  = accountService.read(transaction.getFrom());
		if(from.getBalance().compareTo(transaction.getValue())==-1)
			throw new InsufficientFundsException("Not enough funds to perform this operation");
		Account to = accountService.read(transaction.getTo());

		BigDecimal value = transaction.getValue();
		from.setBalance(from.getBalance().subtract(value));
		to.setBalance(to.getBalance().add(value));
		accountService.update(from);
		accountService.update(to);
	}

	/**
	 * actual rule for a deposit
	 * @param transaction
	 * @throws UnrecognizedEntityException 
	 */
	private void deposit(Transaction transaction) throws UnrecognizedEntityException{
		Account to = accountService.read(transaction.getTo());
		to.setBalance(to.getBalance().add(transaction.getValue()));
		accountService.update(to);
	}
	
	public List<Transaction> listByAccount(Account account){
		return dao.listByAccount(account);
	}
}

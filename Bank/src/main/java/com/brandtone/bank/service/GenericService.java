/**
 * 
 */
package com.brandtone.bank.service;

import java.util.List;


/**
 * @author Ricardo Siqueira
 *
 * @param <D>
 */
public interface GenericService<E> {

	public abstract E create(E entity) throws Exception;

	public abstract E read(E entity);

	public abstract E update(E entity);

	public abstract void delete(E entity);
	
	public List<E> listALL();

}
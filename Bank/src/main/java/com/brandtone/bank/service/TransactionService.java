/**
 * 
 */
package com.brandtone.bank.service;

import java.util.List;

import com.brandtone.bank.entity.Account;
import com.brandtone.bank.entity.Transaction;

/**
 * @author Ricardo Siqueira
 *
 */
public interface TransactionService extends GenericService<Transaction> {
	
	public List<Transaction> listByAccount(Account account);

}
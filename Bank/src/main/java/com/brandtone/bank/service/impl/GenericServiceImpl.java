/** 
 * 
 */
package com.brandtone.bank.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.brandtone.bank.dao.GenericDAO;
import com.brandtone.bank.entity.Entity;
import com.brandtone.bank.exception.UnrecognizedEntityException;
import com.brandtone.bank.service.GenericService;

/**
 * @author Ricardo Siqueira
 *
 */
@Service
// @Transactional //Make along with a future persistence layer
public abstract class GenericServiceImpl<E extends Entity> implements GenericService<E>  {
	
	private GenericDAO<E> dao;
	 
    public GenericServiceImpl(GenericDAO<E> superDao) {
        this.dao=superDao;
    }
 
    public GenericServiceImpl() {
    }	
	public E create(E entity) throws Exception {
		try {
			return dao.create(entity);
		} catch (UnrecognizedEntityException e) {
			e.printStackTrace();
			return null;
		}
	}
	public E read(E entity){
		try {
			return dao.read(entity);
		} catch (UnrecognizedEntityException e) {
			e.printStackTrace();
			return null;
		}
	}

	public E update(E entity) {
		try {
			return dao.update(entity);
		} catch (UnrecognizedEntityException e) {
			e.printStackTrace();
			return null;
		}
	}

	public void delete(E entity) {
		dao.delete(entity);
	}
	
	@Override
	public List<E> listALL() {
		return dao.listAll();
	}

}

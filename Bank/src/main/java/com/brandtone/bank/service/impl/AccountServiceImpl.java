/**
 * 
 */
package com.brandtone.bank.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.brandtone.bank.dao.GenericDAO;
import com.brandtone.bank.entity.Account;
import com.brandtone.bank.service.AccountService;

/**
 * @author Ricardo Siqueira
 *
 */
@Service
@Qualifier("accountServiceImpl")
public class AccountServiceImpl extends GenericServiceImpl<Account> implements AccountService {

	@Autowired
    public AccountServiceImpl(@Qualifier("accountDAO") GenericDAO<Account> superDao) {
        super(superDao);
    }
}

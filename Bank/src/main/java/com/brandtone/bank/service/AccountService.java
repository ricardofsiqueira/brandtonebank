/**
 * 
 */
package com.brandtone.bank.service;

import com.brandtone.bank.entity.Account;

/**
 * @author Ricardo Siqueira
 *
 */
public interface AccountService extends GenericService<Account> {

}
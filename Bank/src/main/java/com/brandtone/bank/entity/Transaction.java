package com.brandtone.bank.entity;

import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import org.joda.time.DateTime;

import com.brandtone.bank.constant.TransactionType;

/**
 * Entity representing a bank transaction
 * 
 * @author Ricardo Siqueira
 *
 */
public class Transaction extends Entity {

	
	private DateTime date;
	
	@NotNull
	private TransactionType type;
	
	@NotNull
	private BigDecimal value;

	private Account from;

	@NotNull
	private Account to;

	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public TransactionType getType() {
		return type;
	}

	public void setType(TransactionType type) {
		this.type = type;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public Account getFrom() {
		return from;
	}

	public void setFrom(Account from) {
		this.from = from;
	}

	public Account getTo() {
		return to;
	}

	public void setTo(Account to) {
		this.to = to;
	}

}

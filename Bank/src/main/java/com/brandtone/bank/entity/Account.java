package com.brandtone.bank.entity;

import java.math.BigDecimal;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Entity representing a bank account
 * 
 * @author Ricardo Siqueira
 * 
 */
@RequestMapping("Account")
public class Account extends Entity{
	
    @NotEmpty
	private String name;

    @NotEmpty
	private String address;
    
	private Integer phoneNumber;

	private BigDecimal balance;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(Integer phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
}

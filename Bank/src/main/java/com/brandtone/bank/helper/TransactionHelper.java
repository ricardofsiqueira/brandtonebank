/**
 * 
 */
package com.brandtone.bank.helper;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.brandtone.bank.constant.TransactionType;
import com.brandtone.bank.entity.Transaction;

/**
 * @author Ricardo Siqueira
 *
 */
public class TransactionHelper { 
	private String value;
	private String fromTo;
	private String date;
	private String type;

	public TransactionHelper(Transaction t, Integer accountId){
		this.value = (t.getType().equals(TransactionType.DEPOSIT)?"+":"-")+t.getValue().toString();
		
		fromTo = t.getTo()!=null && t.getTo().getId()==accountId ? 
				t.getFrom().getId()+"-"+t.getFrom().getName() :
				t.getTo().getId()+"-"+t.getTo().getName();
				
				DateTimeFormatter dtf = DateTimeFormat.forPattern("dd/MM/yyyy");
				date = dtf.print(t.getDate());

				type = t.getType().name();

	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}


	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFromTo() {
		return fromTo;
	}

	public void setFromTo(String fromTo) {
		this.fromTo = fromTo;
	}

}

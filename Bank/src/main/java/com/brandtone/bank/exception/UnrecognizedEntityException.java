package com.brandtone.bank.exception;

/**
 * Exception to be thrown when an account have not enough fund to perform an
 * operation
 * 
 * @author Ricardo Siqueira
 * 
 */
public class UnrecognizedEntityException extends Exception {

	private static final long serialVersionUID = 9021272817343211214L;

	public UnrecognizedEntityException() {
	}

	public UnrecognizedEntityException(String message) {
		super(message);
	}

	public UnrecognizedEntityException(Throwable cause) {
		super(cause);
	}

	public UnrecognizedEntityException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnrecognizedEntityException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
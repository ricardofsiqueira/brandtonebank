package com.brandtone.bank.exception;

/**
 * Exception to be thrown when an account have not enough fund to perform an
 * operation
 * 
 * @author Ricardo Siqueira
 * 
 */
public class InsufficientFundsException extends Exception {

	private static final long serialVersionUID = 9021272817343211214L;

	public InsufficientFundsException() {
	}

	public InsufficientFundsException(String message) {
		super(message);
	}

	public InsufficientFundsException(Throwable cause) {
		super(cause);
	}

	public InsufficientFundsException(String message, Throwable cause) {
		super(message, cause);
	}

	public InsufficientFundsException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
package com.brandtone.bank.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.brandtone.bank.dao.AccountDAO;
import com.brandtone.bank.entity.Account;

@Repository
@Qualifier("accountDAO")
@SuppressWarnings("unchecked")
public class AccountDAOImpl extends GenericDAOImpl<Account> implements AccountDAO {

	@Override
	public List<Account> listAll() {
		return (List<Account>) storage.listAll(Account.class);
	}
}

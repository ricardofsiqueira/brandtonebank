/**
 * 
 */
package com.brandtone.bank.dao;

import com.brandtone.bank.entity.Account;

/**
 * @author Ricardo Siqueira
 *
 */
public interface AccountDAO extends GenericDAO<Account>{

}
package com.brandtone.bank.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.brandtone.bank.dao.GenericDAO;
import com.brandtone.bank.entity.Entity;
import com.brandtone.bank.exception.UnrecognizedEntityException;
import com.brandtone.bank.storage.Storage;

@SuppressWarnings("unchecked")
@Repository
public abstract class GenericDAOImpl<E extends Entity> implements GenericDAO<E> {
	
	@Autowired
	protected Storage storage;
	
	@Override
	public E create(E entity) throws UnrecognizedEntityException{
		return (E)storage.save(entity);
	}
	
	@Override
	public E read(E entity) throws UnrecognizedEntityException{
		return (E)storage.get(entity);
	}
	
	@Override
	public E update(E entity) throws UnrecognizedEntityException{
		return (E)storage.merge(entity);
	}
	
	@Override
	public void delete(E entity){
		//TODO implement
	}
		
	
}

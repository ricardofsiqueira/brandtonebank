/**
 * 
 */
package com.brandtone.bank.dao;

import java.util.List;

import com.brandtone.bank.entity.Entity;
import com.brandtone.bank.exception.UnrecognizedEntityException;

/**
 * @author Ricardo Siqueira
 *
 * @param <E>
 */
public interface GenericDAO<E extends Entity> {

	public abstract E create(E entity) throws UnrecognizedEntityException;

	public abstract E read(E entity) throws UnrecognizedEntityException;

	public abstract E update(E entity) throws UnrecognizedEntityException;

	public abstract void delete(E entity);
	
	public abstract List<E> listAll();

}
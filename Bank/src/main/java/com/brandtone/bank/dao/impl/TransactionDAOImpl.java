package com.brandtone.bank.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.brandtone.bank.dao.TransactionDAO;
import com.brandtone.bank.entity.Account;
import com.brandtone.bank.entity.Transaction;

@Repository
@Qualifier("transactionDAO")
public class TransactionDAOImpl extends GenericDAOImpl<Transaction> implements TransactionDAO {
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Transaction> listAll() {
		return (List<Transaction>) storage.listAll(Transaction.class);
	}
	
	public List<Transaction> listByAccount(Account account){
		List<Transaction> list = new ArrayList<Transaction>();
		Integer id = account.getId();
		for(Transaction t: listAll())
			if(t.getTo()!=null && t.getTo().getId().equals(id)
				||t.getFrom()!=null && t.getFrom().getId().equals(id))
				list.add(t);
		return list;
	} 
	
	
}

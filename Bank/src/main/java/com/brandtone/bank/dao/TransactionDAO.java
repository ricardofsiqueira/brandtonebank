/**
 * 
 */
package com.brandtone.bank.dao;

import java.util.List;

import com.brandtone.bank.entity.Account;
import com.brandtone.bank.entity.Transaction;

/**
 * @author Ricardo Siqueira
 *
 */
public interface TransactionDAO extends GenericDAO<Transaction> {
	
	public List<Transaction> listByAccount(Account account);

}
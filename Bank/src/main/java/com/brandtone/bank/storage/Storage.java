package com.brandtone.bank.storage;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.brandtone.bank.entity.Account;
import com.brandtone.bank.entity.Entity;
import com.brandtone.bank.entity.Transaction;
import com.brandtone.bank.exception.UnrecognizedEntityException;

/**
 * 
 * Class simulates a database layer. Store objects in runtime memory but doesn't have persistence. 
 * 
 * @author Ricardo Siqueira
 *
 */
@Component
public class Storage {
	
	private static Map<Integer,Account> tableAccount = new HashMap<Integer,Account>();
	private static Map<Integer,Transaction> tableTransaction = new HashMap<Integer,Transaction>();

	private int indexAccount=0;
	private int indexTransaction=0;
	
	/**
	 * Simulates a drop database
	 */
	public void clean(){
		tableAccount.clear();
		 tableTransaction.clear();
	}
	
	/**
	 * equivalent to persist() in JPA
	 * @return Account included in map
	 */
	private Entity save(Account account){
		if(account == null)
			return null;
		if(account.getId()==null||account.getId()==0)
			account.setId(++indexAccount);
		if(account.getBalance()==null)
			account.setBalance(new BigDecimal(00.00));
		tableAccount.put(account.getId(), account);
		return account;
		
	}
	

	/**
	 * equivalent to persist() in JPA
	 * @return Transaction included in map
	 */
	private Entity save(Transaction transaction){
		if(transaction == null)
			return null;
		if(transaction.getId()==null||transaction.getId()==0)
			transaction.setId(++indexTransaction);
		tableTransaction.put(transaction.getId(), transaction);
		return transaction;
	}
	
	/**
	 * Generic save
	 * @param entity
	 * @return
	 * @throws UnrecognizedEntityException 
	 */
	public Entity save(Entity entity) throws UnrecognizedEntityException{
		if(entity instanceof Transaction)
			return save((Transaction)entity);
		if(entity instanceof Account)
			return save((Account)entity);
		throw new UnrecognizedEntityException();
	}
	
	private Entity getAccountById(Integer id){
		if(id==null)
			return null;
		return tableAccount.get(id);
	}
	
	private Entity getTransactionById(Integer id){
		if(id==null)
			return null;
		return tableTransaction.get(id);
	}
	
	/**
	 * Equivalent to find() of JPA
	 * @param id
	 * @return Entity found
	 * @throws UnrecognizedEntityException 
	 */
	public Entity get(Entity entity) throws UnrecognizedEntityException{
		if(entity instanceof Account)
			return getAccountById(entity.getId());
		if(entity instanceof Transaction)
			return getTransactionById(entity.getId());
		throw new UnrecognizedEntityException("Non recognized or non implemented entity");
	}
	
	
	private Entity merge(Account account){
		if(account.getId()==null)
			return null;
		tableAccount.put(account.getId(), account);
		return account;
	}

	private Entity merge(Transaction transaction){
		if(transaction.getId()==null)
			return null;
		tableTransaction.put(transaction.getId(), transaction);
		return transaction;
	}
	

	/**
	 * Equivalent to merge() of JPA
	 * @param id
	 * @return Entity updated
	 * @throws UnrecognizedEntityException 
	 */
	public Entity merge(Entity entity) throws UnrecognizedEntityException{
		if(entity instanceof Account)
			return merge((Account)entity);
		if(entity instanceof Transaction)
			return merge((Transaction)entity);
		throw new UnrecognizedEntityException();
	}
	
	public List<?> listAll(Class<?> clazz){
		if(clazz == Account.class){
			List<Account> list = new ArrayList<Account>();
			list.addAll(tableAccount.values());
			return list;
		}
		if(clazz == Transaction.class){
			List<Transaction> list = new ArrayList<Transaction>();
			list.addAll(tableTransaction.values());
			return list;
		}
		return null;
	}
	
	public static void main(String[] args) {
		Storage s = new Storage();
		s.listAll(Account.class);
	}
}

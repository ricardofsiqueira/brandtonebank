/**
 * 
 */
package com.brandtone.bank.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.brandtone.bank.entity.Entity;
import com.brandtone.bank.service.GenericService;

/**
 * @author Ricardo Siqueira
 *
 */
@Controller
public abstract class GenericController<T extends Entity> {

	public abstract GenericService<T> getService();

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView mav = new ModelAndView(getView()+"List");
		mav.addObject("list", getService().listALL());
		return mav;
	}

	protected abstract String getView();

	@RequestMapping(value = "/createUpdate", method = RequestMethod.POST)
	protected ModelAndView createOrUpdate(@Valid @ModelAttribute("entity") T entity, BindingResult bindingResult) {
		ModelAndView mav = new ModelAndView(getView()+"Edit");
		if(bindingResult.hasErrors()){
			entity.setId(0);
			return mav;
		}
		if(entity.getId()!=null && entity.getId()!=0)
			entity = getService().update(entity);
		else
			try {
				getService().create(entity);
			} catch (Exception e) {
				e.printStackTrace();
				bindingResult.addError(new ObjectError(e.getCause().toString(),e.getMessage()) );
				mav.addObject("entity",entity);
				return mav;
			}
		return list();
	}


	@RequestMapping(value = "/open/{id}", method = RequestMethod.GET)
	@ModelAttribute
	public ModelAndView open(@PathVariable Integer id, @ModelAttribute T entity, ModelAndView mav){
		entity.setId(id);
		if(id!=0)
			entity = getService().read((T)entity);
		mav.setViewName(getView()+"Edit");
		mav.addObject("entity",entity);
		return mav;
	}

}
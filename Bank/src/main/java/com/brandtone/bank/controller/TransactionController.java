/**
 * 
 */
package com.brandtone.bank.controller;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.brandtone.bank.constant.TransactionType;
import com.brandtone.bank.entity.Account;
import com.brandtone.bank.entity.Transaction;
import com.brandtone.bank.service.AccountService;
import com.brandtone.bank.service.GenericService;
import com.brandtone.bank.service.TransactionService;

/**
 * @author Ricardo Siqueira
 *
 */
@Controller("TransactionController")
@RequestMapping("/Transaction")
public class TransactionController extends GenericController<Transaction> {

	private String view = "transaction";
	
	@Autowired
	TransactionService transactionService;
	
	@Autowired
	AccountService accountService;
	
	@Override
	public GenericService<Transaction> getService() {
		return transactionService;
	}

	@Override
	protected String getView() {
		return view;
	}
	
	@Override
	@RequestMapping(value = "/createUpdate", method = RequestMethod.POST)
	protected ModelAndView createOrUpdate(Transaction entity,BindingResult bindingResult) {
		if(entity.getType().equals(TransactionType.TRANSFER) && entity.getFrom()==null)
			bindingResult.addError(new ObjectError("from", "must not be empty"));
		else
			entity.setDate(new DateTime());
		return super.createOrUpdate(entity, bindingResult);
	}
	
	@Override
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		view="account";
		return super.list();
	}
	
	@Override
	@RequestMapping(value = "/open/{id}", method = RequestMethod.GET)
	@ModelAttribute
	public ModelAndView open(@PathVariable Integer id, @ModelAttribute Transaction entity, ModelAndView mav) {
		mav.addObject("accounts",accountService.listALL());
		mav.addObject("types",TransactionType.values());
		Account ac = new Account();
		ac.setId(id);
		entity.setTo(accountService.read(ac));
		return super.open(0, entity, mav);
	}

}

/** 
 * 
 */
package com.brandtone.bank.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.brandtone.bank.entity.Account;
import com.brandtone.bank.entity.Transaction;
import com.brandtone.bank.helper.TransactionHelper;
import com.brandtone.bank.service.AccountService;
import com.brandtone.bank.service.GenericService;
import com.brandtone.bank.service.TransactionService;

/**
 * @author Ricardo Siqueira
 *
 */
@Controller("AccountController")
@RequestMapping("/Account")
public class AccountController extends GenericController<Account> {

	@Autowired
	AccountService accountService;
	
	@Autowired
	TransactionService transactionService;
	
	@Override
	public GenericService<Account> getService() {
		return accountService;
	}

	@Override
	protected String getView() {
		return "account";
	}
	
	@Override
	@RequestMapping(value = "/open/{id}", method = RequestMethod.GET)
	@ModelAttribute
	public ModelAndView open(@PathVariable Integer id, Account entity, ModelAndView mav) {
		Account account = new Account();
		account.setId(id);
		List<TransactionHelper> trans = new ArrayList<TransactionHelper>(); 
		for (Transaction transaction : transactionService.listByAccount(account)) 
			trans.add(new TransactionHelper(transaction,id));
		
		mav.addObject("statement", trans);
		return super.open(id, entity, mav);
	}

}
package com.brandtone.bank.constant;

public enum TransactionType {
	DEPOSIT, TRANSFER;
}

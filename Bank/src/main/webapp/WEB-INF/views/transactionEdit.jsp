<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 

<%@ page isELIgnored="false"%>  
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>

<html>
<body>
<form:form action="${pageContext.request.contextPath}/Transaction/createUpdate" modelAttribute="entity" method="post">
<a href="${pageContext.request.contextPath}/Account/open/${entity.to.id}">Back</a>
	<h2>Transaction</h2>
		<fieldset>
			<legend>Transaction</legend>
			<table>
				<tr>
					<td>Type:</td>
					<td><form:select path="type" onchange="showHideFrom(this.value);">
						<form:option value="" label="--- Select ---"/>
   						<form:options items="${types}"/>
   					</form:select>	<td>
			 		<td><form:errors path="type" cssStyle="color: #ff0000;"/></td>
				</tr>
				<tr>
					<td>From:</td>
					<td><form:select path="from" id="from" disabled="true">
						<form:option value="" label="--- Select ---"/>
   						<form:options items="${accounts}" itemLabel="name" />
   						</form:select></td>	
			 		<td><form:errors path="from" cssStyle="color: #ff0000;"/></td>
				</tr>
				<tr>
						<td>To:</td>
						<td><form:select path="to">
							<form:option value="" label="--- Select ---"/>
	   						<form:options items="${accounts}" itemLabel="name" />
	   					</form:select></td>	
				 		<td><form:errors path="to" cssStyle="color: #ff0000;"/></td>
				</tr>
				<tr>
					<td>Value:</td>
					<td><form:input path="value" onkeypress="return numDot(this.id);" /></td>
					<td><form:errors path="value" cssStyle="color: #ff0000;"/></td>
				</tr>
			</table>
		</fieldset>
		<input type="submit" value="Save"/>
		<br>
		<br>
</form:form>
</body>
<script type="text/javascript">
 function numDot(txt){
            if(event.keyCode > 47 && event.keyCode < 58 || event.keyCode == 46){
               var amount = document.getElementById(txt).value;
               var present=0;
               var count=0;

               if(amount.indexOf(".",present)||amount.indexOf(".",present+1));
               do{
               present=amount.indexOf(".",present);
               if(present!=-1){
                 count++;
                 present++;
                }
               }
               while(present!=-1);
               if(present==-1 && amount.length==0 && event.keyCode == 46){
                    event.keyCode=0;
                    return false;
               }

               if(count>=1 && event.keyCode == 46){
                    event.keyCode=0;
                    return false;
               }
               if(count==1){
                var lastdigits=amount.substring(amount.indexOf(".")+1,amount.length);
                if(lastdigits.length>=2){
                    event.keyCode=0;
                    return false;
                 }
               }
                    return true;
            }
            else{
                    event.keyCode=0;
                    return false;
            }

        }
 	function showHideFrom(value){
 		if(value=='TRANSFER')
 			document.getElementById("from").disabled = false;
 		else
 			document.getElementById("from").disabled = true;

 	}
    </script>

</html>
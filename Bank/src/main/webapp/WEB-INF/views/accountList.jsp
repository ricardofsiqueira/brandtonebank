<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>

<html>
<body>
<form:form action="open/0" commandName="entity" method="GET">  
${addAction}
	<h2>Accounts</h2> 
	


		<fieldset>
			<legend>List of Accounts</legend>
			<display:table  name="${list}" id="acc" class="class" >
				<display:column style="aligin:center" title="Number" property="id"/>
				<display:column style="aligin:center" title="Name" class="class">
					<a href="open/${acc.id}">${acc.name}</a>
				</display:column>
				<display:column style="aligin:center" title="Address" property="address"/>
				<display:column style="aligin:center" title="Phone" property="phoneNumber"/>
				<display:column style="aligin:center" title="Balance" property="balance"/>
			</display:table>
		</fieldset>
<input type="submit" value="New Account"/>
</form:form>		
</body>
</html>
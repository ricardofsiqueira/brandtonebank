<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %> 

<%@ page isELIgnored="false"%>  
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>

<html>
<body>
<form:form action="${pageContext.request.contextPath}/Account/createUpdate" modelAttribute="entity" method="post">
<a href="${pageContext.request.contextPath}/Account/list">Back</a>
<br>
<br>
<c:if test="${entity.id!=0}">
	<a href="${pageContext.request.contextPath}/Transaction/open/${entity.id}">New Transaction</a>
</c:if>
	<h2>Account ${entity.name}</h2>
		<fieldset>
			<legend>Accounts</legend>
			<table>
				<c:if test="${entity.id!=0}" >
					<tr>
						<td>Number:</td>
						<td><form:input path="id" disabled="true" /></td>
					</tr>
				</c:if> 
				<tr>
					<td>Name:</td>
					<td><form:input path="name" disabled="${entity.id!=0}" /></td>
			 		<td><form:errors path="name" cssStyle="color: #ff0000;"/></td>
				</tr>
				<tr>
					<td>Address:</td>
					<td><form:input path="address" disabled="${entity.id!=0}" /></td>
					<td><form:errors path="address" cssStyle="color: #ff0000;"/></td>
				</tr>
				<tr>
					<td>Phone Number:</td>
					<td><form:input onkeypress='return event.charCode >= 48 && event.charCode <= 57' path="phoneNumber" disabled="${entity.id!=0}" /></td>
					<td><form:errors path="phoneNumber" cssStyle="color: #ff0000;"/></td>
				</tr>
				<tr>
					<td>Balance:</td>
					<td><form:input path="balance" disabled="true" /></td>
				</tr>
			</table>
			
		</fieldset>
		<input type="submit" value="Save"/>
		<br>
		<br>
		<c:if test="${entity.id!=0}" >
			<fieldset>
				<legend>Last Transactions</legend>
				<display:table  name="${statement}" id="trs" class="class" >
					<display:column style="aligin:center" title="Date" property="date"/>
					<display:column style="aligin:center" title="type" property="type"/>
					<display:column style="aligin:center" title="From/To" property="fromTo"/>
					<display:column style="aligin:center" title="Value" property="value"/>
				</display:table>
			</fieldset>
		</c:if>
</form:form>		
</body>
</html>
package com.brandtone.bank.exception;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brandtone.bank.entity.NonRelatedEntity;
import com.brandtone.bank.storage.Storage;

/**
 * @author Ricardo Siqueira
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-context.xml")
public class UnrecognizedEntityTest {

	@Autowired
	private Storage storage;

	@Test(expected=UnrecognizedEntityException.class)
	public void UnrecognizedEntityExceptionSaveTest() throws UnrecognizedEntityException{
		storage.save(new NonRelatedEntity());
	}

	@Test(expected=UnrecognizedEntityException.class)
	public void UnrecognizedEntityExceptionGetTest() throws UnrecognizedEntityException{
		storage.get(new NonRelatedEntity());
	}

	@Test(expected=UnrecognizedEntityException.class)
	public void UnrecognizedEntityExceptionMergeTest() throws UnrecognizedEntityException{
		storage.merge(new NonRelatedEntity());
	}
}
package com.brandtone.bank.dao;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brandtone.bank.constant.TransactionType;
import com.brandtone.bank.entity.Account;
import com.brandtone.bank.entity.Transaction;
import com.brandtone.bank.exception.UnrecognizedEntityException;

/**
 * @author Ricardo Siqueira
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-context.xml")
public class TransactionDAOTest {

	@Autowired
	private TransactionDAO dao;
	private Account from = buildAccount("Account From", "Test From", 123456,new BigDecimal(00.00));
	private Account to = buildAccount("Account To", "Test To", 848484,new BigDecimal(100.00));

	private Account buildAccount(String name, String address, Integer phone,BigDecimal balance) {
		Account account = new Account();
		account.setName(name);
		account.setAddress(address);
		account.setPhoneNumber(phone);
		account.setBalance(balance);
		return account;
	}

	private Transaction buildTransaction(Integer id, DateTime date, TransactionType type,BigDecimal value, Account from, Account to) {
		Transaction transaction = new Transaction();
		transaction.setId(id);
		transaction.setDate(date);
		transaction.setType(type);
		transaction.setValue(value);
		transaction.setFrom(from);
		transaction.setTo(to);
		return transaction;
	}

	@Test
	public void testCreateTransaction() throws UnrecognizedEntityException {
		Transaction deposit = buildTransaction(1,new DateTime(),TransactionType.DEPOSIT, new BigDecimal(100.00), from, null);
		deposit = (Transaction) dao.create(deposit);
		assertNotNull(deposit);
		assertNotNull(deposit.getId());

	}
	
	@Test
	public void testReadTransaction() throws UnrecognizedEntityException {
		Transaction aux = new Transaction();
		aux.setId(1);
		aux= (Transaction) dao.read(aux);
		assertNotNull(aux);
		assertEquals(new Integer(1), aux.getId());
	}
	
	@Test
	public void testUpdateTransaction() throws UnrecognizedEntityException {
		final DateTime date = new DateTime();
		final TransactionType type = TransactionType.TRANSFER;
		final BigDecimal value = new BigDecimal(600.00);
		Transaction aux = buildTransaction(1, date, type, value, to, from);
		
		aux = (Transaction) dao.update(aux);
		assertNotNull(aux);
		assertEquals(date, aux.getDate());
		assertEquals(type, aux.getType());
		assertEquals(value, aux.getValue());
		assertEquals(from, aux.getTo());
		assertEquals(to, aux.getFrom());

	}

	
}
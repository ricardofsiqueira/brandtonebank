package com.brandtone.bank.controller;

 import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.servlet.ModelAndView;

import com.brandtone.bank.entity.Transaction;
import com.brandtone.bank.exception.InsufficientFundsException;
import com.brandtone.bank.exception.UnrecognizedEntityException;
import com.brandtone.bank.initializer.DataInitializer;

@ContextConfiguration("/test-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class TransactionControllerTest {
	
	@Autowired
	private DataInitializer dataInitializer;
	
	@Autowired
	private TransactionController transactionController;
		
	@Before
	public void prepare() throws InsufficientFundsException, UnrecognizedEntityException {
		dataInitializer.prepare();
	}
	
	@Test
	public void listTest() {
		ModelAndView mav = transactionController.list();
		assertEquals("transactionList",mav.getViewName());
		
		@SuppressWarnings("unchecked")
		List<Transaction> transactions = (List<Transaction>) mav.getModelMap().get("list");
		assertNotNull(transactions);		
		assertEquals(DataInitializer.TOTAL,transactions.size());		
	}
	
	
	public void newTest() {
		ModelAndView mav = transactionController.open(0,new Transaction(), new ModelAndView());
		assertNotNull(mav);
		assertEquals("transactionEdit", mav.getViewName());
		Object object = mav.getModel().get("entity");
		assertTrue(Transaction.class.isAssignableFrom(object.getClass()));
		Transaction ac = (Transaction) object;
		assertNull(ac.getId());
	}

	@Test
	public void editTest() {
		ModelAndView mav = transactionController.open(new Integer(2),new Transaction(), new ModelAndView());
		assertNotNull(mav);
		assertEquals("transactionEdit", mav.getViewName());
		Object object = mav.getModel().get("entity");
		assertTrue(Transaction.class.isAssignableFrom(object.getClass()));
		Transaction ac = (Transaction) object;
		assertEquals(new Integer(2),ac.getId());
	}
	
}

package com.brandtone.bank.controller;

 import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.servlet.ModelAndView;

import com.brandtone.bank.entity.Account;
import com.brandtone.bank.exception.InsufficientFundsException;
import com.brandtone.bank.exception.UnrecognizedEntityException;
import com.brandtone.bank.initializer.DataInitializer;

@ContextConfiguration("/test-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class AccountControllerTest {
	
	@Autowired
	private DataInitializer dataInitializer;
	
	@Autowired
	private AccountController accountController;
		
	@Before
	public void prepare() throws InsufficientFundsException, UnrecognizedEntityException {
		dataInitializer.prepare();
	}
	
	@Test
	public void listTest() {
		ModelAndView mav = accountController.list();
		assertEquals("accountList",mav.getViewName());
		
		@SuppressWarnings("unchecked")
		List<Account> accounts = (List<Account>) mav.getModelMap().get("list");
		assertNotNull(accounts);		
		assertEquals(DataInitializer.TOTAL,accounts.size());		
	}
	
	
	public void newTest() {
		ModelAndView mav = accountController.open(0,new Account(), new ModelAndView());
		assertNotNull(mav);
		assertEquals("accountEdit", mav.getViewName());
		Object object = mav.getModel().get("entity");
		assertTrue(Account.class.isAssignableFrom(object.getClass()));
		Account ac = (Account) object;
		assertNull(ac.getId());
	}

	@Test
	public void editTest() {
		ModelAndView mav = accountController.open(new Integer(2),new Account(), new ModelAndView());
		assertNotNull(mav);
		assertEquals("accountEdit", mav.getViewName());
		Object object = mav.getModel().get("entity");
		assertTrue(Account.class.isAssignableFrom(object.getClass()));
		Account ac = (Account) object;
		assertEquals(new Integer(2),ac.getId());
	}
	
}

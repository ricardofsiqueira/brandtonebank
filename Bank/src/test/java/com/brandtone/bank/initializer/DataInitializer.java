/**
 * 
 */
package com.brandtone.bank.initializer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.brandtone.bank.constant.TransactionType;
import com.brandtone.bank.entity.Account;
import com.brandtone.bank.entity.Transaction;
import com.brandtone.bank.exception.InsufficientFundsException;
import com.brandtone.bank.exception.UnrecognizedEntityException;
import com.brandtone.bank.storage.Storage;

/**
 * @author Ricardo Siqueira
 *
 */
@Component
@Scope("prototype")
public class DataInitializer {
	
	public static final int TOTAL=3;

	@Autowired
	private Storage storage;

	public List<Long> people = new ArrayList<Long>();

	private Account buildAccount(Integer id, String name, String address, Integer phone,BigDecimal balance) throws InsufficientFundsException, UnrecognizedEntityException {
		Account account = new Account();
		account.setName(name);
		account.setAddress(address);
		account.setPhoneNumber(phone);
		account.setBalance(balance);
		storage.save(account);
		return account;
	}

	private Transaction buildTransaction(Integer id, DateTime date, TransactionType type,BigDecimal value, Account from, Account to) throws UnrecognizedEntityException {
		Transaction transaction = new Transaction();
		transaction.setId(id);
		transaction.setDate(date);
		transaction.setType(type);
		transaction.setValue(value);
		transaction.setFrom(from);
		transaction.setTo(to);
		storage.save(transaction);
		return transaction;
	}

	public void prepare() throws InsufficientFundsException, UnrecognizedEntityException{
//		storage.clean();
		DateTime date;
		date = new DateTime();
		Account one = buildAccount(1,"Account One", "Test One", 123456,new BigDecimal(100.00));
		Account two = buildAccount(2,"Account Two", "Test Two", 848484,new BigDecimal(00.00));
		Account three = buildAccount(3,"Account Three", "Test Three", 848484,new BigDecimal(200.00));
		buildTransaction(1, date, TransactionType.DEPOSIT, new BigDecimal(00.00), one, one);
		buildTransaction(2, date, TransactionType.TRANSFER, new BigDecimal(20.00), one, two);
		buildTransaction(3, date, TransactionType.DEPOSIT, new BigDecimal(20.00), three, three);
	}
	
}

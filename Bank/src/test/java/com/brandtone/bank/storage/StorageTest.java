package com.brandtone.bank.storage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brandtone.bank.constant.TransactionType;
import com.brandtone.bank.entity.Account;
import com.brandtone.bank.entity.Transaction;
import com.brandtone.bank.exception.UnrecognizedEntityException;

/**
 * @author Ricardo Siqueira
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-context.xml")
public class StorageTest {

	@Autowired
	private Storage storage;

	private Account buildAccount(String name, String address, Integer phone,BigDecimal balance) {
		Account account = new Account();
		account.setName(name);
		account.setAddress(address);
		account.setPhoneNumber(phone);
		account.setBalance(balance);
		return account;
	}

	private Transaction buildTransaction(DateTime date, TransactionType type,
			BigDecimal value, Account from, Account to) {
		Transaction transaction = new Transaction();
		transaction.setDate(date);
		transaction.setType(type);
		transaction.setValue(value);
		transaction.setFrom(from);
		transaction.setTo(to);
		return transaction;
	}

	@Test
	public void testAccount() throws UnrecognizedEntityException {
		Account account = buildAccount("TestAccount", "Test Address", 123456,
				new BigDecimal(123.45));
		account = (Account) storage.save(account);
		assertNotNull(account);
		assertNotNull(account.getId());

		Account aux = (Account) storage.get(account);
		assertNotNull(aux);
		assertEquals(account.getId(), aux.getId());

		final String name = "New Name";
		final String address = "New Address";
		final Integer phone = 321;
		final BigDecimal balance = new BigDecimal(543.21);
		aux.setName(name);
		aux.setAddress(address);
		aux.setBalance(balance);
		aux.setPhoneNumber(phone);
		aux = (Account) storage.merge(aux);
		assertNotNull(aux);
		assertEquals(name, aux.getName());
		assertEquals(address, aux.getAddress());
		assertEquals(phone, aux.getPhoneNumber());
		assertEquals(balance, aux.getBalance());

	}

	@Test
	public void testTransaction() throws UnrecognizedEntityException {
		Account from = buildAccount("Account From", "Test From", 123456,
				new BigDecimal(00.00));
		Account to = buildAccount("Account To", "Test To", 789123,
				new BigDecimal(00.00));
		Transaction deposit = buildTransaction(new DateTime(),
				TransactionType.DEPOSIT, new BigDecimal(100.00), from, null);
		deposit = (Transaction) storage.save(deposit);
		assertNotNull(deposit);
		assertNotNull(deposit.getId());

		Transaction transfer = buildTransaction(new DateTime(),
				TransactionType.TRANSFER, new BigDecimal(20.00), from, to);
		transfer = (Transaction) storage.save(transfer);
		assertNotNull(transfer);
		assertNotNull(transfer.getId());

		Transaction aux = (Transaction) storage.get(transfer);
		assertNotNull(aux);
		assertEquals(transfer.getId(), aux.getId());

		final DateTime date = new DateTime();
		final TransactionType type = TransactionType.DEPOSIT;
		final BigDecimal value = new BigDecimal(600.00);
		from = aux.getTo();
		to = aux.getFrom();
		aux.setDate(date);
		aux.setType(type);
		aux.setValue(value);
		aux.setFrom(from);
		aux.setTo(to);
		aux = (Transaction) storage.merge(aux);
		assertNotNull(aux);
		assertEquals(date, aux.getDate());
		assertEquals(type, aux.getType());
		assertEquals(value, aux.getValue());
		assertEquals(from, aux.getFrom());
		assertEquals(to, aux.getTo());

	}
}
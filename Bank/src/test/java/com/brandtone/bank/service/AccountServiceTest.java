package com.brandtone.bank.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brandtone.bank.entity.Account;

/**
 * @author Ricardo Siqueira
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-context.xml")
public class AccountServiceTest {

	@Autowired
	private AccountService service;

	private Account buildAccount(Integer id, String name, String address, Integer phone,BigDecimal balance) {
		Account account = new Account();
		account.setName(name);
		account.setAddress(address);
		account.setPhoneNumber(phone);
		account.setBalance(balance);
		return account;
	}


	@Test
	public void testCreateAccount() throws Exception {
		Account account = buildAccount(1,"TestAccount", "Test Address", 123456,new BigDecimal(123.45));
		account = (Account) service.create(account);
		assertNotNull(account);
		assertNotNull(account.getId());

		Account aux = (Account) service.read(account);
		assertNotNull(aux);
		assertEquals(account.getId(), aux.getId());

		final String name = "New Name";
		final String address = "New Address";
		final Integer phone = 321;
		final BigDecimal balance = new BigDecimal(543.21);
		aux.setName(name);
		aux.setAddress(address);
		aux.setBalance(balance);
		aux.setPhoneNumber(phone);
		aux = (Account) service.update(aux);
		assertNotNull(aux);
		assertEquals(name, aux.getName());
		assertEquals(address, aux.getAddress());
		assertEquals(phone, aux.getPhoneNumber());
		assertEquals(balance, aux.getBalance());

	}
	
	@Test
	public void testReadAccount() {
		Account aux = new Account();
		aux.setId(1);
		aux = (Account) service.read(aux);
		assertNotNull(aux);
		assertEquals(new Integer(1), aux.getId());
	}
	
	
	public void testUpdateAccount() {
		final String name = "New Name";
		final String address = "New Address";
		final Integer phone = 321;
		final BigDecimal balance = new BigDecimal(543.21);
		Account aux = buildAccount(1, name, address, phone, balance);
		aux = (Account) service.update(aux);
		assertNotNull(aux);
		assertEquals(name, aux.getName());
		assertEquals(address, aux.getAddress());
		assertEquals(phone, aux.getPhoneNumber());
		assertEquals(balance, aux.getBalance());
	}

}
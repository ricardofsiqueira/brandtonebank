package com.brandtone.bank.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.List;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.brandtone.bank.constant.TransactionType;
import com.brandtone.bank.entity.Account;
import com.brandtone.bank.entity.Transaction;

/**
 * @author Ricardo Siqueira
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/test-context.xml")
public class TransactionServiceTest {

	@Autowired
	private AccountService accountService;

	@Autowired
	private TransactionService service;
	private Account from; 
	private Account to;
	private DateTime date;

	@Before
	public void prepare() throws Exception{
		date = new DateTime();
		from = buildAccount(1,"Account From", "Test From", 123456,new BigDecimal(100.00));
		to = buildAccount(2,"Account To", "Test To", 848484,new BigDecimal(00.00));
		Transaction transaction = buildTransaction(1, date, TransactionType.DEPOSIT, new BigDecimal(00.00), from, to);
		service.create(transaction);
	}

	private Account buildAccount(Integer id, String name, String address, Integer phone,BigDecimal balance) throws Exception {
		Account account = new Account();
		account.setName(name);
		account.setAddress(address);
		account.setPhoneNumber(phone);
		account.setBalance(balance);
		accountService.create(account);
		return account;
	}

	private Transaction buildTransaction(Integer id, DateTime date, TransactionType type,BigDecimal value, Account from, Account to) {
		Transaction transaction = new Transaction();
		transaction.setId(id);
		transaction.setDate(date);
		transaction.setType(type);
		transaction.setValue(value);
		transaction.setFrom(from);
		transaction.setTo(to);
		return transaction;
	}

	@Test
	public void testDeposit() throws Exception {
		BigDecimal value = new BigDecimal(100.00);
		BigDecimal valueBefore = to.getBalance();
		Transaction deposit = buildTransaction(2,date,TransactionType.DEPOSIT,value, null, to);
		deposit = (Transaction) service.create(deposit);
		Account after = accountService.read(to);
		assertNotNull(deposit);
		assertEquals(after.getBalance(), valueBefore.add(value));

	}

	@Test(expected=Exception.class)
	public void testException() throws Exception {
		BigDecimal value = new BigDecimal(200.00);
		BigDecimal valueBeforeTo = to.getBalance();
		BigDecimal valueBeforeFrom = from.getBalance();
		Transaction transfer = buildTransaction(3,date,TransactionType.TRANSFER, value, from, to);
		transfer = (Transaction) service.create(transfer);
		assertNotNull(transfer);
		Account afterTo = accountService.read(to);
		Account afterFrom = accountService.read(from);
		assertEquals(afterTo.getBalance(), valueBeforeTo.add(value));
		assertEquals(afterFrom.getBalance(), valueBeforeFrom.subtract(value));
	}


	@Test
	public void testTransfer() throws Exception {
		BigDecimal value = new BigDecimal(20.00);
		BigDecimal valueBeforeTo = to.getBalance();
		BigDecimal valueBeforeFrom = from.getBalance();
		Transaction transfer = buildTransaction(4,date,TransactionType.TRANSFER, value, from, to);
		transfer = (Transaction) service.create(transfer);
		assertNotNull(transfer);
		Account afterTo = accountService.read(to);
		Account afterFrom = accountService.read(from);
		assertEquals(afterTo.getBalance(), valueBeforeTo.add(value));
		assertEquals(afterFrom.getBalance(), valueBeforeFrom.subtract(value));
	}

	@Test
	public void testReadTransaction() {
		Transaction aux = new Transaction();
		aux.setId(1);
		aux= (Transaction) service.read(aux);
		assertNotNull(aux);
		assertEquals(new Integer(1), aux.getId());
	}

	@Test
	public void testUpdateTransaction() {
		final TransactionType type = TransactionType.TRANSFER;
		final BigDecimal value = new BigDecimal(600.00);
		Transaction aux = buildTransaction(1,date, type, value, to, from);

		aux = (Transaction) service.update(aux);
		assertNotNull(aux);
		assertEquals(date, aux.getDate());
		assertEquals(type, aux.getType());
		assertEquals(value, aux.getValue());
		assertEquals(from, aux.getTo());
		assertEquals(to, aux.getFrom());
	}

	@Test
	public void testListAll(){
		List<Transaction> list = service.listALL();
		assertNotNull(list);
		assertFalse(list.isEmpty());
	}

}